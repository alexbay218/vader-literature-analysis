from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

modernFile = open("texts/modern.txt", "r")
modernOut = open("texts/modern.out", "w")
modernArr = modernFile.read().split("\n")
dataArr = []

print(modernArr)

analyzer = SentimentIntensityAnalyzer()
index = 0
for sentence in modernArr:
    vs = analyzer.polarity_scores(sentence)
    modernOut.write("{:-<65} | {}\n".format(str(vs), sentence))
    dataArr.append(vs)
    print(vs)
    index = index + 1
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

sithoaagFile = open("texts/sithoaag.txt", "r")
sithoaagOut = open("texts/sithoaag.out", "w")
sithoaagArr = sithoaagFile.read().replace("? ", ". ").replace("\n","").split(". ")
dataArr = []

print(sithoaagArr)

analyzer = SentimentIntensityAnalyzer()
index = 0
for sentence in sithoaagArr:
    if len(sentence) > 50:
        vs = analyzer.polarity_scores(sentence)
        sithoaagOut.write("{:-<65} | {}\n".format(str(vs), sentence))
        dataArr.append(vs)
        print(vs)
        index = index + 1
